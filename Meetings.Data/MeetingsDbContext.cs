namespace Meetings.Data
{
    using Models;
    using System.Data.Entity;

    public class MeetingsDbContext : DbContext
    {
        public MeetingsDbContext()
            : base("name=MeetingsDbContext")
        {
        }

        public virtual DbSet<Meeting> Meetings { get; set; }
    }
}