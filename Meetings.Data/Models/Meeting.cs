﻿namespace Meetings.Data.Models
{
    using System;

    public class Meeting
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Location { get; set; }

        public int GuestsCount { get; set; }

        public DateTime StartedAt { get; set; }

        public DateTime EndedAt { get; set; }
    }
}
