﻿namespace Meetings.Data.Repository
{
    using Models;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    public class MeetingRepository
    {
        private readonly MeetingsDbContext db;

        public MeetingRepository()
        {
            this.db = new MeetingsDbContext();
        }

        public IEnumerable<Meeting> All()
        {
            return this.db.Meetings.ToList();
        }

        public Meeting Get(int id)
        {
            return this.db.Meetings.FirstOrDefault(m => m.Id == id);
        }

        public void Insert(Meeting meeting)
        {
            this.db.Meetings.Add(meeting);
            this.db.SaveChanges();
        }

        public void Update(Meeting meeting)
        {
            this.db.Entry(meeting).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void Delete(Meeting meeting)
        {
            this.db.Meetings.Remove(meeting);
            this.db.SaveChanges();
        }

        public void DeleteById(int id)
        {
            var meeting = this.db
                .Meetings
                .FirstOrDefault(m => m.Id == id);

            this.db.Meetings.Remove(meeting);
            this.db.SaveChanges();
        }
    }
}
