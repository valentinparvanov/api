﻿namespace Meetings.Api.Controllers
{
    using Data.Models;
    using Data.Repository;
    using System.Web.Http;

    public class MeetingsController : ApiController
    {
        private readonly MeetingRepository repository;

        public MeetingsController()
        {
            this.repository = new MeetingRepository();
        }

        public IHttpActionResult Get()
            => Ok(this.repository.All());

        public IHttpActionResult Get(int id)
        {
            var meeting = this.repository.Get(id);

            if (meeting == null)
            {
                return NotFound();
            }

            return Ok(meeting);
        }

        public IHttpActionResult Post(Meeting meeting)
        {
            this.repository.Insert(meeting);

            return Ok(meeting);
        }

        public IHttpActionResult Put(Meeting meeting)
        {
            this.repository.Update(meeting);

            return Ok(meeting);
        }


        public IHttpActionResult Delete(int id)
        {
            var meeting = this.repository.Get(id);

            if (meeting == null)
            {
                return NotFound();
            }

            repository.Delete(meeting);

            return Ok();
        }
    }
}